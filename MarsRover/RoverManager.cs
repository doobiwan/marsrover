﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRover
{
    public class RoverManager
    {
        public IList<Rover> Rovers { get; private set; }

        public void LoadInstructions(string instructions)
        {
            var instructionList = new List<string>();

            using (var reader = new StringReader(instructions.Trim()))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    instructionList.Add(line.Trim());
                }
            }

            LoadInstructions(instructionList);
        }
        public void LoadInstructions(IList<string> instructionList)
        {
            Rovers = new List<Rover>();
            var maxPosition = GetPosition(instructionList[0]);
            try
            {
                for (int i = 1; i < instructionList.Count; i += 2)
                    Rovers.Add(new Rover(maxPosition, GetPosition(instructionList[i]), instructionList[i + 1]));
            }
            catch (Exception)
            {
                throw new ArgumentException(string.Format("instructionList is not valid."));
            }
        }

        private Position GetPosition(string lineOne)
        {
            try
            {
                var coordinates = lineOne.Split(' ');
                var orientation = (coordinates.Length == 3)
                                        ? "NESW".IndexOf(coordinates[2])
                                        : 0;
                return new Position
                {
                    X = int.Parse(coordinates[0]),
                    Y = int.Parse(coordinates[1]),
                    OrientationIndex = (uint)orientation
                };
            }
            catch (Exception)
            {
                throw new ArgumentException(string.Format("'{0}' is not a valid starting argument.", lineOne));
            }
        }

        public void MoveRovers()
        {
            foreach (var rover in Rovers)
                rover.Move();

        }
    }
}
