﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarsRover;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace MarsRover.Tests
{
    [TestClass()]
    public class RoverTests
    {
        [TestMethod()]
        public void MoveTest_Simple()
        {
            var max = new Position { X = 5, Y = 5, OrientationIndex = 0 };
            var expected = new Position { X = 2, Y = 4, OrientationIndex = 2 };
            var movement = "RMLMMRMLMMRR";

            var rover = new Rover(max);

            rover.Move(movement);

            rover.CurrentPosition.X.Should().Be(expected.X);
            rover.CurrentPosition.Y.Should().Be(expected.Y);
            rover.CurrentPosition.OrientationIndex.Should().Be(expected.OrientationIndex);
        }

        [TestMethod()]
        public void MoveTest_Exceed_Bounds()
        {
            var max = new Position { X = 5, Y = 5, OrientationIndex = 0 };
            var expected = new Position { X = 5, Y = 0, OrientationIndex = 2 };
            var movement = "MMMMMMMMMLMMMMRRMMMMMMMRMMMMMMM";

            var rover = new Rover(max);

            rover.Move(movement);

            rover.CurrentPosition.X.Should().Be(expected.X);
            rover.CurrentPosition.Y.Should().Be(expected.Y);
            rover.CurrentPosition.OrientationIndex.Should().Be(expected.OrientationIndex);
        }
    }
}
