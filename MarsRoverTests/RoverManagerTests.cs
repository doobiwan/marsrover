﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarsRover;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace MarsRover.Tests
{
    [TestClass()]
    public class RoverManagerTests
    {
        [TestMethod()]
        public void MoveRoversTest()
        {
            var expectedFirst = new Position { X = 1, Y = 3, OrientationIndex = 0 };
            var expectedLast = new Position { X = 5, Y = 1, OrientationIndex = 1 };

            var target = new RoverManager();
            target.LoadInstructions(Instructions);
            target.MoveRovers();

            target.Rovers.First().CurrentPosition.X.Should().Be(expectedFirst.X);
            target.Rovers.First().CurrentPosition.Y.Should().Be(expectedFirst.Y);
            target.Rovers.First().CurrentPosition.Orientation.Should().Be(expectedFirst.Orientation);

            target.Rovers.Last().CurrentPosition.X.Should().Be(expectedLast.X);
            target.Rovers.Last().CurrentPosition.Y.Should().Be(expectedLast.Y);
            target.Rovers.Last().CurrentPosition.Orientation.Should().Be(expectedLast.Orientation);
        }

        public string Instructions
        {
            get
            {
                return @"5 5
                        1 2 N
                        LMLMLMLMM
                        3 3 E
                        MMRMMRMRRM            
                        ";
            }
        }
    }
}
