﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRover
{
    public class Rover
    {
        private string movement;

        public Rover(Position max, Position start = null, string movement = null)
        {
            CurrentPosition = start ?? new Position();
            MaxPosition = max;
            this.movement = movement;
        }

        public Position CurrentPosition { get; private set; }
        public Position MaxPosition { get; private set; }

        public Position Move(string movement)
        {
            this.movement = movement;
            return Move();
        }
        public Position Move()
        {
            if (movement == null)
                throw new ArgumentNullException("null movements. For no movements specifty an empty string");

            var movements = movement.Trim().ToArray();

            foreach (var move in movements)
            {
                switch (move)
                {
                    case 'L':
                        CurrentPosition.OrientationIndex = (4 + CurrentPosition.OrientationIndex - 1) % 4;
                        break;
                    case 'R':
                        CurrentPosition.OrientationIndex = (4 + CurrentPosition.OrientationIndex + 1) % 4;
                        break;
                    case 'M':
                        Advance();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("'{0}' is not a valid movement instruction", move));
                }
            }

            return CurrentPosition;
        }

        private void Advance()
        {
            switch (CurrentPosition.OrientationIndex)
            {
                case 0:
                    if (CurrentPosition.Y < MaxPosition.Y)
                        CurrentPosition.Y++;
                    break;
                case 1:

                    if (CurrentPosition.X < MaxPosition.X)
                        CurrentPosition.X++;
                    break;
                case 2:
                    if (CurrentPosition.Y > 0)
                        CurrentPosition.Y--;
                    break;
                case 3:
                    if (CurrentPosition.X > 0)
                        CurrentPosition.X--;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("'{0}' is an invalid orientation", CurrentPosition.OrientationIndex));
            }
        }
    }
}
