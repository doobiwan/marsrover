﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MarsRover.Desktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnExecute_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var manager = new RoverManager();
                manager.LoadInstructions(txtProgram.Text);
                manager.MoveRovers();

                txtProgram.Text = "Output:" + Environment.NewLine;

                foreach (var rover in manager.Rovers)
                    txtProgram.Text += string.Format("{0} {1} {2}\n", rover.CurrentPosition.X, rover.CurrentPosition.Y, rover.CurrentPosition.Orientation);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
